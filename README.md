# shikra_programmer

https://github.com/Xipiter/shikra-programmer

http://www.xipiter.com/uploads/2/4/4/8/24485815/shikra_documentation.pdf

######################################################

# Shikra EEPROM Programming

## Requirements

1. Install Libusb: `sudo apt-get install libusb-dev` on linux. OSX can skip this step.
2. Install pyusb python libusb bindings: `sudo pip3 install pyusb`
3. Run Shikra programming utility: `sudo ./shikra.py` Root access is needed to be able to Read and Write to the Shikra USB Device.

## Using LED programming methods

The Shikra programming utility allows users to enable the Shikra LED under different configurations. These methods are called `set_led_*`
Warning: This may not work with older Shikra devices.

1. Run utility: `sudo ./shikra.py`
2. Find attached Shikra device with `find_shikra`
3. Set LED configuration with `set_led_*`
4. Write config to Shikra EEPROM with `write_config`

## Utility Output

```
[+] Welcome to the SHIKRA programming utility by XIPITER.

  ██████  ██░ ██  ██▓ ██ ▄█▀ ██▀███   ▄▄▄
▒██    ▒ ▓██░ ██▒▓██▒ ██▄█▒ ▓██ ▒ ██▒▒████▄
░ ▓██▄   ▒██▀▀██░▒██▒▓███▄░ ▓██ ░▄█ ▒▒██  ▀█▄
  ▒   ██▒░▓█ ░██ ░██░▓██ █▄ ▒██▀▀█▄  ░██▄▄▄▄██
▒██████▒▒░▓█▒░██▓░██░▒██▒ █▄░██▓ ▒██▒ ▓█   ▓██▒
▒ ▒▓▒ ▒ ░ ▒ ░░▒░▒░▓  ▒ ▒▒ ▓▒░ ▒▓ ░▒▓░ ▒▒   ▓▒█░
░ ░▒  ░ ░ ▒ ░▒░ ░ ▒ ░░ ░▒ ▒░  ░▒ ░ ▒░  ▒   ▒▒ ░
░  ░  ░   ░  ░░ ░ ▒ ░░ ░░ ░   ░░   ░   ░   ▒
      ░   ░  ░  ░ ░  ░  ░      ░           ░  ░
      
shikra> help

Documented commands (type help <topic>):
========================================
find_shikra  help

Undocumented commands:
======================
EOF  exit

shikra> find_shikra
[+] Looking for Shikra...
[+] Shikra device found.
  CONFIGURATION 1: 500 mA ==================================
   bLength              :    0x9 (9 bytes)
   bDescriptorType      :    0x2 Configuration
   wTotalLength         :   0x20 (32 bytes)
   bNumInterfaces       :    0x1
   bConfigurationValue  :    0x1
   iConfiguration       :    0x0 
   bmAttributes         :   0x80 Bus Powered
   bMaxPower            :   0xfa (500 mA)
    INTERFACE 0: Vendor Specific ===========================
     bLength            :    0x9 (9 bytes)
     bDescriptorType    :    0x4 Interface
     bInterfaceNumber   :    0x0
     bAlternateSetting  :    0x0
     bNumEndpoints      :    0x2
     bInterfaceClass    :   0xff Vendor Specific
     bInterfaceSubClass :   0xff
     bInterfaceProtocol :   0xff
     iInterface         :    0x2 Single RS232-HS
      ENDPOINT 0x81: Bulk IN ===============================
       bLength          :    0x7 (7 bytes)
       bDescriptorType  :    0x5 Endpoint
       bEndpointAddress :   0x81 IN
       bmAttributes     :    0x2 Bulk
       wMaxPacketSize   :  0x200 (512 bytes)
       bInterval        :    0x0
      ENDPOINT 0x2: Bulk OUT ===============================
       bLength          :    0x7 (7 bytes)
       bDescriptorType  :    0x5 Endpoint
       bEndpointAddress :    0x2 OUT
       bmAttributes     :    0x2 Bulk
       wMaxPacketSize   :  0x200 (512 bytes)
       bInterval        :    0x0

shikra programming> help

Documented commands (type help <topic>):
========================================
backup         help                 set_led_off  set_led_tx    zero
dump           print_config         set_led_on   set_led_txrx
factory_reset  restore_from_backup  set_led_rx   write_config

Undocumented commands:
======================
EOF  exit

shikra programming>
```
